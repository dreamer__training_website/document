# errors

|code|msg|comment|
|:--:|:---:|:---|
|101: |"parameter invalid:" | #参数错误|
|1011: |"required parameter absent" | #参数缺失|
|1012: |"未定义操作" | #不可进行的操作|
|102: |"authorization error:" | #权限错误|
|1021: |"not login yet" | #未登录报错|
|103: |"authentication error" | #认证错误|
|1031: |"passport invalid" | #用户不存在|
|1032: |"password incorrect" | #密码错误|
|104: |"resource unavailable" | #资源不可用|
|1041: |"not found: " | #资源未找到|
|201: |"server unavailable" | #服务器不可用，比如关机了/连不上什么的|
|202: |"server failure" | #服务失败|
|203: |"service not ready" | #服务暂未启用，比如后台还没写好|
|204: |"unknown server error" | #服务器出现了奇怪的问题|
|3001: |"file system error" | #文件系统错误|

# activity

1. get all info of activity by acticityID via post

获取指定活动信息

2. get all acticityID, name, status via post

获取所有活动信息

3. generate activity by userID(leader), all params of activity exclude members (finished: |false by default) via post

生成活动

4. delete by activityID via delete

删除活动

5. modify as generate via post

修改活动信息

6. remove member by acticityID, userID via delete

移除成员

7. quit activity by userID, activityID

退出活动

8. 活动是否重复 名称&日期



# user

 1. login by passport(username | email | telephone), password via post

 登陆

 2. register by username,password,email,telephone,realname,QQ,college,token(email) via post


 注册

 2.x:

 用户名是否重复

 邮箱是否重复

 电话是否重复

 token 是否正确

 3. get current user info via post

 当前用户信息

 4. get user info by userID via post

 指定用户信息

 5. get user info by userID list via post

 批量获取用户信息

 <!-- 6. isSuperAdmin by userID via post

 是否管理员 -->

 7. logout via post

 登出

 8. verify identity password by passport, password
 
 验证已登陆用户密码

 9. modify password by userID, password
 
 修改密码

 10. send email token by passport

 发送邮箱验证码确认身份

 11. modify user info by avatar,description,QQ,telephone
 
 修改用户信息


# application

1. send application by userID, activityID, description

申请加入活动

2. confirm application by applicationID, status(1:通过,2:拒绝)

处理申请

3. get applications by activityID

获取所有申请

4. get applications not confirmed by acticityID

获取所有未确认申请

5. get applications refused

获取所有被拒绝申请

6. get applications passed

获取所有已通过申请

